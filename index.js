// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages (requests/responses).
// The message sent by the client is called "request".]
// The message sent by the server as an answer is called "response".
// Require Directive --> load a particular module node.js

let http = require("http"); //HTTP - Hyper Text Transfer Protocol
// http is a node module/application
// it consist different components

let port = 3000;
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
// createServer() is a method of the http object responsible for creating a server using Node.js

http.createServer(function (request, response){
	// writeHead
	// code - 200 --> OK, No Problem, Success
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello World!");
}).listen(port);

// A port is a virtual point where network connections start and end.
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server
// 3000, 4000, 5000, 8000 - Usually used for web development

console.log(`Server is running at localhost: ${port}`);
