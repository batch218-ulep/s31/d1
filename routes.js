

let http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
	//first endpoint is /greeting --> "http://localhost:4000/greeting"
	if (request.url == "/greeting"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Hello Again!");

	}else if (request.url == "/"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("You are now in my Homepage!");

	}else{
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("PAGE NOT FOUND");
	}
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);

